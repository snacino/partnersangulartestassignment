import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';
import { IPartner } from './models/IPartners';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'AmgularTestAssignment';

  appService: AppService

  constructor(appService: AppService) {
    this.appService = appService
  }
  ngOnInit(): void {
    const partnersJson = localStorage.getItem('partners')
    if (partnersJson) {
      const partners: IPartner[] = JSON.parse(partnersJson)
      this.appService.partners = partners
    }
  }

  reset() {
    this.appService.reset()
  }
}
