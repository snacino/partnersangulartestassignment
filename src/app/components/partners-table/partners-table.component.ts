import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';

// Local imports
import { DialogComponent } from '../dialog/dialog.component';
import { IPartner } from '../../models/IPartners'
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'partners-table',
  templateUrl: './partners-table.component.html',
  styleUrls: ['./partners-table.component.css'],
})
export class PartnersTableComponent implements OnInit {

  displayedColumns: string[] = ['NAZIV_PARTNERA', 'PARTNER_OIB', 'MJESTO', 'ADRESA', 'ACTIONS'];
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  tableDataSource!: MatTableDataSource<IPartner>;
  dialog: MatDialog;

  appService: AppService;

  constructor(appService: AppService, dialog: MatDialog) {
    this.dialog = dialog;
    this.appService = appService
  }

  ngOnInit() {}

  ngAfterViewInit() {
    this.tableDataSource = new MatTableDataSource<IPartner>(this.appService.partners!)
        this.tableDataSource.filterPredicate = (data: IPartner, filter: string) => {
          return data?.MJESTO?.toLocaleLowerCase().startsWith(filter.toLowerCase()) || false
        }
        this.tableDataSource.sort = this.sort
        this.tableDataSource.paginator = this.paginator;
  }

  // Filter table
  onKeyUp(eventValue: string) {
    this.tableDataSource.filter = eventValue
  }
  
  //Delete selected row
  deleteAction(selectedParnter: IPartner) {
    let dialogRef = this.dialog.open(DialogComponent, {
      data: { ...selectedParnter },
    })
    dialogRef.afterClosed().subscribe((partner: IPartner) => {
      if (partner) {
        this.appService.deletePartner(partner.PARTNER_OIB)
        this.setupTable()
      }
    })
  }

  //Setup paginator,datasource,sort
  setupTable(){
    this.tableDataSource = new MatTableDataSource<IPartner>(this.appService.partners!)
    this.tableDataSource.filterPredicate = (data: IPartner, filter: string) => {
      return data?.MJESTO?.toLocaleLowerCase().startsWith(filter.toLowerCase()) || false
    }
    this.tableDataSource.sort = this.sort
    this.tableDataSource.paginator = this.paginator;
  }

  // Check if data is valid (OIB,NAZIV_PARTNERA)
  checkIfValid(element: any, col: string) {
    if (col === "NAZIV_PARNTERA") {
      return element?.[col] !== "" || element?.[col] !== undefined
    }
    if (col === "PARTNER_OIB") {
      return !isNaN(parseInt(element?.[col]))
    }
    return true;
  }

  uploadData(){
    let invalidPartners = this.appService.partners?.filter((partner: IPartner) => {
      return partner.NAZIV_PARTNERA === undefined || partner.NAZIV_PARTNERA === '' || isNaN(parseInt(partner.PARTNER_OIB))
    })
    if (invalidPartners && invalidPartners.length > 0) {
      alert("Some or more partners have invalid values")
    } else {
      this.appService.insertPartnersToDb();
    }
  }
}
