import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { AppService } from 'src/app/app.service';

// Local Imports
import { FileUploadService } from './file-upload.service';


@Component({
  selector: 'file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css'],
  providers:[AppService]
})
export class FileUploadComponent {
  uploadedFile?: Blob;
  appService: AppService;
  fileUploadService: FileUploadService;
  
  
  constructor(appService: AppService, fileUploadService: FileUploadService) {
    this.appService = appService;
    this.fileUploadService = fileUploadService
  }
  
  onFileInputChange(evt: any){
    this.uploadedFile = evt?.target?.files?.[0]
  }

  onUpload(){ 
    if (this.uploadedFile) {
      this.fileUploadService.readExcele(this.uploadedFile);
    } else {
      alert("Please upload a file first.")
    }
  }
}
