import { EventEmitter, Output } from '@angular/core';
import { Injectable } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { IPartner } from 'src/app/models/IPartners';
import * as XLSX from 'xlsx';


@Injectable({
  providedIn: 'root',
})

export class FileUploadService {

  appService: AppService;
  constructor(appService: AppService) {
    this.appService = appService;
  }

  readExcele(uploadedFile: Blob) {
    // Put excele read in separate function
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });

      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      let partners: IPartner[] = XLSX.utils.sheet_to_json(ws, { header: 0 });
      if (partners) {
        // Send notification that partners changed
        this.appService.partnersDataChanged(partners)
      }
    }
    reader.readAsBinaryString(uploadedFile)
  }
}