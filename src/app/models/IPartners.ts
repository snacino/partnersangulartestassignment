export interface IPartner {
    SIFRA_PARTNERA: string,
    NAZIV_PARTNERA: string,
    PARTNER_OIB: string,
    MJESTO?: string,
    ADRESA?: string, 
}