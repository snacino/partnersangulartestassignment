import { HttpClient, HttpHeaders } from '@angular/common/http';;
import { Injectable } from '@angular/core';

//Local imports
import { IPartner } from 'src/app/models/IPartners';

@Injectable({
    providedIn: 'root',
})

//Singleton Service used in components. 
export class AppService {

    partners?: IPartner[]
    private httpClient: HttpClient
    constructor(httpClient: HttpClient) {
        this.httpClient = httpClient
    }

    partnersDataChanged(partners: IPartner[]) {
        this.partners = partners
        this.saveToBrowsereStorage()
    }

    deletePartner(partnerOib: any) {
        let filteredPartners = this.partners?.filter((data: IPartner) => {
            // Should be unique for every partner
            return data.PARTNER_OIB !== partnerOib
        })
        if (filteredPartners) {
            this.partners = filteredPartners;
            this.saveToBrowsereStorage()
        }
    }

    saveToBrowsereStorage() {
        localStorage.setItem('partners', JSON.stringify(this.partners))
    }
    reset() {
        localStorage.removeItem('partners');
        this.partners = undefined
        this.resetData()
    }


    // HTTP SERVICE
    // TODO: Move this to http service
    insertPartnersToDb() {
        const jsonObject: any = [];
        // TODO: Rewrite this, post does not accept same key as in partners
        this.partners?.forEach((elem: IPartner) => {
            jsonObject.push({
                name: elem.NAZIV_PARTNERA,
                pid: elem.PARTNER_OIB,
                city: elem.MJESTO,
                dress: elem.ADRESA
            })
        })
        let jsonString = JSON.stringify(jsonObject)
        this.httpClient.post<IPartner>('http://localhost:5000/api/partner',
            jsonString,
            {
                headers:
                    new HttpHeaders(
                        {
                            'Content-Type': 'application/json',
                        }
                    )
            })
            .subscribe(data => {
                console.log("Respone handling not implemented")
            })
    }

    resetData() {
        this.httpClient.delete('http://localhost:5000/api/partner/truncate')
        .subscribe(data => {
            console.log("Respone handling Not implemented")
        })
    }
}